package kz.aitu.oop.examples.assignment5;

import lombok.Data;

@Data

    public class Circle extends Shape{

        private double radius;

        public Circle() {
            this.radius = 1.0;
        }

        public Circle(double rad) {
            this.radius = rad;
        }

        public Circle(double rad, String x, boolean y) {
            super(x, y);
            this.radius = rad;
        }

        public double getArea(){
            return (this.radius * this.radius) * 3.14;
        }

        public double getPerimeter() {
            return (this.radius * 2) * 3.14;
        }

        @Override
        public String toString(){
            return "A Circle with radius=" + this.radius + "which a subclass of" + super.toString();
        }


    }


