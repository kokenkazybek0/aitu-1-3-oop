package kz.aitu.oop.examples.assignment5;

import lombok.Data;

@Data
public class Rectangle extends Shape{

    private double width;
    private double length;

    public Rectangle() {
        this.width = 1.0;
        this.length = 1.0;
    }

    public Rectangle(double w, double l) {
        this.width = w;
        this.length = l;
    }

    public Rectangle(double w, double l, String x, boolean y) {
        super(x, y);
        this.width = w;
        this.length = l;
    }

    public double getArea(){
        return this.width * this.length;
    }

    public double getPerimeter() {
        return (this.length + this.length) * 2;
    }

    @Override
    public String toString(){
        return "A Rectangle with width=" + this.width + " and length=" + this.length+", which is a subclass of" +super.toString();
    }


}

