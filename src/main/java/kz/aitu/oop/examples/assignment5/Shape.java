package kz.aitu.oop.examples.assignment5;

import lombok.Data;

@Data
public class Shape {

    private String color;
    private boolean filled;

    public Shape() {
        this.color = "green";
        this.filled = true;
    }

    public Shape(String x, boolean y) {
        this.color = x;
        this.filled = y;
    }

    public String filled() {

        if (this.filled) {
            return "filled";
        }
        else
            {
            return "not filled";
        }
    }

    public String toString(){
        return "A Shape with color of " + color + " and " + filled();
    }


}
