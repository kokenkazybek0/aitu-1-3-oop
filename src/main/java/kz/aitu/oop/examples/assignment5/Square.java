package kz.aitu.oop.examples.assignment5;

public class Square extends Rectangle {

    public Square() {
        super(1.0, 1.0);
    }

    public Square(double a) {
        super(a,a);
    }

    public Square(double a, String x, boolean y) {
        super(a, a, x, y);

    }

    @Override
    public void setLength(double a) {
        super.setLength(a);
        super.setWidth(a);
    }

    @Override
    public void setWidth(double a) {
        super.setLength(a);
        super.setWidth(a);
    }

    @Override
    public String toString(){
        return "A Square with side= " + getLength() + ", which a subclass of" + super.toString();
    }
}
