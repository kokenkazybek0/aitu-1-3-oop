package kz.aitu.oop.examples.assignment3;

import static java.util.Arrays.sort;

public class MySpecialString {
    private int[] a;

    // keep the array values internally without duplicated values public
    MySpecialString(int[] value) {

        sort(value);
        int[] arr = new int[value.length];
        int x = 0;
        for (int i = 0; i < value.length; i++ ) {
            if (i == 0) {

                arr[x] = value[i];  
                x++;
            } else if (value[i] != value[i-1]) {
                arr[x] = value[i];
                x++;
            }
        }
        a = new int[x];
        for (int i = 0; i < x; i++) {
            a[i] = arr[i];
        }
    }
    // return the number of values that are stored
    public int length() {
        return a.length;
    }
    //  return the value stored at position or -1 if position is not available
    public int value(int pos) {
        if (pos >= 0 && pos < a.length) {

            return a[pos];
        } else {
            return -1;
        }
    }
    // return true if value is stored, otherwise false
    public boolean contain(int value) {

        for (int i = 0; i < length(); i++ ) {

            if(a[i] == value) {
                return true;
            }

        }

        return false;
    }



    // count for how many time value is stored
    public int count(int value) {
        int x = 0;
        for (int i = 0; i < length(); i++ ) {
            if(a[i] == value) {
                x++;
            }
        }
        return x;
    }

    //print the stored values ... }
    public void printValue() {
        for (int i = 0; i < a.length; i++ ) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
    }

}
