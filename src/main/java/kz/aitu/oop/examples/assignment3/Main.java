package kz.aitu.oop.examples.assignment3;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int[] arr = new int[a];

        for (int i = 0; i < a; i++ ){

            arr[i] = sc.nextInt();
        }

        MySpecialString osa = new MySpecialString(arr);
        osa.printValue();
    }


}
