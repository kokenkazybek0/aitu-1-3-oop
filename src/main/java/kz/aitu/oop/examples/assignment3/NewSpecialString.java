package kz.aitu.oop.examples.assignment3;
import static java.util.Arrays.sort;

public class NewSpecialString {

    private String[] a;

    // keep the array values internally with/without duplicated values

    public NewSpecialString(String[] value) {
        sort(value);
        String[] arr = new String[value.length];
        int x = 0;

        for (int i = 0; i < value.length; i++ ) {
            if (i == 0) {
                arr[x] = value[i];
                x++;
            }
            else if (value[i] != value[i-1]) {
                arr[x] = value[i];
                x++;
            }
        }
        a = new String[x];
        for (int i = 0; i < x; i++) {
            a[i] = arr[i];
        }
    }

    // return the number of values(strings) that are stored
    public int length() {
        return a.length;
    }

    // return the value stored at position or -1 if position is not available
    public String value(int pos) {
        if (pos >= 0 && pos < a.length) {
            return a[pos];
        } else {
            return "-1";
        }
    }
    // return true if value is stored, otherwise false
    public boolean contain(String value) {
        for (int i = 0; i < length(); i++ ) {
            if(a[i] == value) {
                return true;
            }
        }
        return false;
    }
    // count for how many time value is stored
    public int count(String value) {
        int x = 0;
        for (int i = 0; i < length(); i++ ) {
            if(a[i] == value) {
                x++;
            }
        }
        return x;
    }
    //print the stored values ...

    public void printValue() {
        for (int i = 0; i < a.length; i++ ) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
    }

}
